USE ecc_app;
GO

-- Dropping all sequences
DROP SEQUENCE bndl_seq;
DROP SEQUENCE broadcast_status_seq;
DROP SEQUENCE CHECK_SEQ;
DROP SEQUENCE chq_sequence_seq;
DROP SEQUENCE coll_seq;
DROP SEQUENCE envelope_seq;
DROP SEQUENCE hibernate_sequence;
DROP SEQUENCE HOLIDAY_SEQ;
DROP SEQUENCE INT_TRANSACTION_SEQ;
DROP SEQUENCE report_definition_id_seq;
DROP SEQUENCE report_parameters_id_seq;
DROP SEQUENCE report_schedule_id_seq;
DROP SEQUENCE reports_execution_logs_id_seq;
DROP SEQUENCE seq_commendpoint;
DROP SEQUENCE seq_commmessage;
DROP SEQUENCE seq_commmessages_history;
GO
